<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\CategoryRepository;
use App\Repository\CommentRepository;
use App\Repository\NewsRepository;
use App\Service\NavCategory;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
    private $navCategory;
    public function __construct(NavCategory $navCategory)
    {
        
    $this->navCategory=$navCategory;

    }




    #[Route('/', name: 'app_home_page')]
    public function index( NewsRepository $newsRepository): Response
    {

        return $this->render('home_page/index.html.twig', [
         'news'=>$newsRepository->findAll(),
         'categoryList'=> $this->navCategory->Category()
        ]);
    }
    #[Route('/news/{id<[0-9]+>}', name: 'app_new_show')]
    public function nwesShow($id, NewsRepository $newsRepository,Request $request, EntityManagerInterface $entityManager, CommentRepository $commentRepository): Response
    {
        $nwesId= $newsRepository->find($id);
        $comment = new  Comment();
        $form = $this->createForm(CommentType::class,$comment);
        $form->handleRequest($request);

         if($form->isSubmitted() && $form->isValid()){
           $_user = $this->getUser();
           $comment->setAuthor($_user);
           $comment->setNews($nwesId);
           $comment->setCreatedAt(new \dateTime);
           $comment->setUpdatedAt(new \dateTime);
           $entityManager->persist($comment);
           $entityManager->flush();
           $this->addFlash('success','your comment has been saved');


         }



        $nwesId= $newsRepository->find($id);
        return $this->render('home_page/nwes_single.html.twig', [
         'single_news'=>$newsRepository->find($nwesId),
         'categoryList'=> $this->navCategory->Category(),
         'form'=>$form->createView(),
         'comments'=>$commentRepository->findBy(['news'=>$nwesId])
        ]);
    }
    #[Route('/news/{id<[0-9]+>}/category', name: 'app_new_by_category_show')]
    public function NewsByCategory($id,NewsRepository $newsRepository,CategoryRepository $categoryRepository)
    {
$idCategory = $categoryRepository->find($id);
$categoryName = $categoryRepository->findOneBy(['id'=>$id]);
$newsByCategory = $newsRepository->findBy(['category'=>$idCategory],[]);

return $this->render('home_page/newsByCtegory.html.twig', [
    'news'=>$newsByCategory,
    'categoryList'=> $this->navCategory->Category(),
    'category'=>$categoryName
   ]);


    }
}
